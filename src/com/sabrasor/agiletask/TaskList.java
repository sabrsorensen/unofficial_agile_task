package com.sabrasor.agiletask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Toast;

public class TaskList extends ExpandableListActivity {

	SimpleExpandableListAdapter expListAdapter;
	List<ArrayList<HashMap<String, String>>> childList;
	int group, child, type;
	static final String[] listNames = {
		"Today",
		"Icebox"
	};
	
	String[][] tasks = {
			{"Hello World", "Tags brah",
			 "Task 2", "Tag 2"},
			{}
	};
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        
        // Capture our button from layout
        Button button = (Button) findViewById(R.id.refresh);
        // Register the onClick listener with the implementation above
        button.setOnClickListener(mAddListener);
        
        ExpandableListView exList = (ExpandableListView) findViewById(android.R.id.list);
        childList = createChildList();
        expListAdapter =
    			new SimpleExpandableListAdapter(
    				this,
    				createGroupList(),	// groupData describes the first-level entries
    				R.layout.child_row,	// Layout for the first-level entries
    				new String[] { "listname" },	// Key in the groupData maps to display
    				new int[] { R.id.listname },		// Data under "colorName" key goes into this TextView
    				childList,	// childData describes second-level entries
    				R.layout.child_row,	// Layout for second-level entries
    				new String[] { "taskName", "tags" },	// Keys in childData maps to display
    				new int[] { R.id.taskname, R.id.tags }	// Data under the keys above go into these TextViews
    			);
    		setListAdapter( expListAdapter );
    		
    		registerForContextMenu(exList);
    }
    private OnClickListener mAddListener = new OnClickListener()
    {
    	public void onClick(View v)
    	{
    		long id = 0;
    		
    		try
    		{
    			Context context = getApplicationContext();
    			CharSequence text = "Updating tasks";
    			int duration = Toast.LENGTH_LONG;
    			
    			Toast toast = Toast.makeText(context,  text,  duration);
    			toast.show();
    		}
    		catch (Exception ex)
    		{
    			Context context = getApplicationContext();
    			CharSequence text = ex.toString() + "ID = " + id;
    			int duration = Toast.LENGTH_LONG;
    			
    			Toast toast = Toast.makeText(context, text, duration);
    			toast.show();
    		}
    	}
    };
    private List<HashMap<String, String>> createGroupList() {
  	  ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
  	  for( int i = 0 ; i < listNames.length ; ++i ) {
  		HashMap<String, String> m = new HashMap<String, String>();
  	    m.put( "listname",listNames[i] );
  		result.add( m );
  	  }
  	  return (List<HashMap<String, String>>)result;
      }
    private List<ArrayList<HashMap<String, String>>> createChildList() {
    	ArrayList<ArrayList<HashMap<String, String>>> result = new ArrayList<ArrayList<HashMap<String, String>>>();
    	for( int i = 0 ; i < tasks.length ; ++i ) {
    // Second-level lists
    	  ArrayList<HashMap<String, String>> secList = new ArrayList<HashMap<String, String>>();
    	  for( int n = 0 ; n < tasks[i].length ; n += 2 ) {
    	    HashMap<String, String> child = new HashMap<String, String>();
    		child.put( "taskName", tasks[i][n] );
    	    child.put( "tags", tasks[i][n+1] );
    		secList.add( child );
    	  }
    	  result.add( secList );
    	}
    	return result;
      }
    @Override  
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {  
    		super.onCreateContextMenu(menu, v, menuInfo); 
    		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;

    		type = ExpandableListView.getPackedPositionType(info.packedPosition);

    		group = ExpandableListView.getPackedPositionGroup(info.packedPosition);

    		child = ExpandableListView.getPackedPositionChild(info.packedPosition);

		  // Only create a context menu for child items
		  if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD){
			  menu.setHeaderTitle("Context Menu");  
			  menu.add(0, v.getId(), 0, "Edit");  
			  menu.add(0, v.getId(), 0, "Delete");
		  }
 }
    @Override
	public boolean onContextItemSelected(MenuItem item) {
       	if(item.getTitle()=="Edit"){editTaskName(item.getItemId());}
    	else if(item.getTitle()=="Delete"){deleteTask(item.getItemId());}
    	else {return false;}
	return true;
	}
    
    public void editTaskName(int i){
    	try
		{
			Context context = getApplicationContext();
			CharSequence text = Integer.toString(i);
			HashMap<String, String> taskHash = childList.get(0).get(child);
  			taskHash.put("taskName", taskHash.get("taskName") + " updated");
  			text = text + taskHash.get("taskName");
  			childList.get(0).remove(child);
  			childList.get(0).add(child, taskHash);
  			expListAdapter.notifyDataSetChanged();
			int duration = Toast.LENGTH_LONG;
			
			Toast toast = Toast.makeText(context,  text,  duration);
			toast.show();
		}
		catch (Exception ex)
		{
			Context context = getApplicationContext();
			CharSequence text = ex.toString() + "ID = " + i;
			int duration = Toast.LENGTH_LONG;
			
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}
    }
    public void deleteTask(int i){
    	try
		{
			Context context = getApplicationContext();
			CharSequence text = Integer.toString(i);
			int duration = Toast.LENGTH_LONG;
			
			Toast toast = Toast.makeText(context,  text,  duration);
			toast.show();
		}
		catch (Exception ex)
		{
			Context context = getApplicationContext();
			CharSequence text = ex.toString() + "ID = " + i;
			int duration = Toast.LENGTH_LONG;
			
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_task_list, menu);
        return true;
    }
}
